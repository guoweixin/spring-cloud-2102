package com.qfjy.service;

import com.qfjy.entity.po.MeetingPub;
import org.apache.ibatis.annotations.Select;

public interface MeetingPubService {

    /**
     * 根据会议编号查询会议信息
     */
    MeetingPub selectMeetingPubByPcode(String pcode);
}
