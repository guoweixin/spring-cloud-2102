package com.qfjy.client;

import com.qfjy.client.impl.VideoInfoControllerFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/*
  @FeignClient 用于通知Feign组件对接口进行代理实现（是由底层Ribbon+RestTemplate)来完成
  name属性我们要调用的是哪个服务（Eureka中的application 名称）
  path:定义当前FeignClient客户端访问统一前缀路径
  fallback: 如果接口服务出现异常/延迟，则运行对应其接口的实现类。
 */
@FeignClient(name = "DUDU-VIDEO",path = "videoInfo",fallback = VideoInfoControllerFallBack.class)
public interface VideoInfoControllerFeignClient {

    /*
    Feign可以把Rest的请求进行隐藏，伪装成类似SpringMVC的Controller一样。
    你不用再自己拼接url，拼接参数等等操作，一切都交给Feign去做。
     */

    @GetMapping("info")    // /info?pcode=11111
    public String selectVideoInfoByPcode(@RequestParam("pcode") String pcode);

}


/**
 * 采用Ribbon+ Http方式 完成通信
 * 如果服务的提供者IP地址或端口号发生改变怎么办？
 *     不再使用ip:端口号 访问地址--》取而代之的是 应用名称(eureka)
 *     这种方式的发送所有的请求都会被ribbon进行拦截，ribbon会通过名称在eureka注册中心
 *     找到对应服务列表， 找到对应的IP:端口号、还会采用负载均衡算法来完成访问


 String url="http://DUDU-VIDEO/videoInfo/info?pcode="+meetingPub.getPcode();
 String result=restTemplate.getForObject(url,String.class);
 */


/**  RestTemplate方式完成通信
 * param1 url 请求的URL地址
 * param2 responseType 希望返回的类型.class
 * param3 占位符 最后一个参数是map,map的key为前面的占位符。map value为参数值
 * 可变长度的参数，一一来替换前面的占位符 name={name}形式，
 * //        String url="http://localhost:8085/videoInfo/info?pcode="+meetingPub.getPcode();
 * //         String result=restTemplate.getForObject(url,String.class);
 */

