package com.qfjy.client.impl;

import com.qfjy.client.VideoInfoControllerFeignClient;
import org.springframework.stereotype.Component;

/**
 * @ClassName VideoInfoControllerFallBack
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/21
 * @Version 1.0
 */
@Component   //Spring IOC容器管理
public class VideoInfoControllerFallBack implements VideoInfoControllerFeignClient {


    @Override
    public String selectVideoInfoByPcode(String pcode) {
        return "视频微服务开小差咯（默认信息）";
    }
}
