package com.qfjy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;


@MapperScan(basePackages = {"com.qfjy.mapper"})
@EnableFeignClients  //启动FeignClient  扫描所有带@FeignClient注解的接口 进行容器管理。
@SpringBootApplication
@EnableEurekaClient   //@EnableDiscoveryClient
@EnableHystrix  //开启熔断器Hystrix注解  @EnableCircuitBreaker

//@SpringCloudApplication  //组合注解  @SpringBootApplication @EnableDiscoveryClient @EnableCircuitBreaker
public class DuduMeetingWeixinApplication {

    public static void main(String[] args) {
        SpringApplication.run(DuduMeetingWeixinApplication.class, args);
    }

}
