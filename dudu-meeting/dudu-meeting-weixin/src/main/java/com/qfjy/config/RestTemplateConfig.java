package com.qfjy.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName RestTemplateConfig
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/19
 * @Version 1.0
 */
@Configuration
public class RestTemplateConfig {

    @Bean
    @LoadBalanced  //Ribbon负载均衡算法
    public RestTemplate restTemplate(){
        return  new RestTemplate();
    }


    @Bean //负载均衡规则改为 RandomRule
    public IRule ribbonRule(){
        return  new RandomRule(); //改为随机策略
    }
}
