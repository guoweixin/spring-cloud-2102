package com.qfjy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableConfigServer  //开启SpringCloud Config配置组件
@SpringBootApplication
@EnableEurekaClient
public class DuduConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(DuduConfigApplication.class, args);
    }

}
