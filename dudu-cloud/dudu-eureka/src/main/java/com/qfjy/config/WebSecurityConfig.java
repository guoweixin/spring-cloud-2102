package com.qfjy.config;

import org.springframework.context.annotation.Configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @ClassName WebSecurityConfig
 * @Description TODO
 * @Author guoweixin
 * @Date 2020/2/15
 * @Version 1.0
 */
@Configuration //注意要加这个注解
@EnableWebSecurity//注意要加这个注解,开启安全配置
public class WebSecurityConfig  extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.csrf().ignoringAntMatchers("/eureka/**");
        //我们的服务注册地址是 在/eureka下⾯面,所以我们可以通过/eureka/**进行忽略安全过滤来达到允许服务注册到⽬目的, 而且其他地址的访问会仍旧需要密码    }
    }

}