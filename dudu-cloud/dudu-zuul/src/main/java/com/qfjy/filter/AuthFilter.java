package com.qfjy.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.qfjy.util.result.ResultCode;
import com.qfjy.util.result.ResultJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName AuthFilter
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/23
 * @Version 1.0
 */
@Component
@Slf4j
public class AuthFilter extends ZuulFilter {


    /**
     * 指定过滤器类型
     - pre过滤器。在请求被路由之前调用。Zuul请求微服务之前。比如请求身份验证，选择微服务实例，记录调试信息等。
     - route过滤器。负责转发请求到微服务。原始请求在此构建，并使用Apache HttpClient或Netflix Ribbon发送原始请求。
     - post过滤器。在route和error过滤器之后被调用。可以在响应添加标准HTTP Header、收集统计信息和指标，以及将响应发送给客户端等。
     - error过滤器。在处理请求发生错误时被调用。
     * @return
     */
    @Override
    public String filterType() {
        // pre
        return FilterConstants.PRE_TYPE;
    }
    /**
      过滤器的执行的Order 顺序。 数字越小优先级越高（Servet/Filter)一致
     */
    @Override
    public int filterOrder() {
        return 0;
    }
    /**
      是否使用该过滤器
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }
    /**
     过滤器执行的具本业务逻辑
     */
    @Override
    public Object run() throws ZuulException {
        System.out.println("AuthFilter认证业务逻辑----》");

        //1 获取请求URL中token参数  （前后端：header/请求参数）
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();

        log.info("请求的址是：" + request.getRequestURL());

        //2、判断acessToekn 值做一个校验 信息是否有效。
        Object objToken = request.getParameter("token");

        //3 如果有效，直接跳到目标服务器发送请求和得到响应
        //4 如果无效，直接返回ajax对象 （用户未登录）
        if (objToken == null){
             log.warn("用户未登录，访问失败，请跳入登录页面");

             //响应体
            requestContext.getResponse().setCharacterEncoding("GBK");

            ObjectMapper objectMapper=new ObjectMapper();
            try {
                String jsonResult=objectMapper.writeValueAsString(new ResultJson<>("用户未登录", ResultCode.NOT_LOGIN));

                requestContext.setResponseBody(jsonResult);

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            return  null;
        }

        log.info("登录成功--》访问目标微服务");

        return null;
    }
}
