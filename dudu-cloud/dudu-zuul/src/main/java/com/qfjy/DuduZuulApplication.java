package com.qfjy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy  //启动Zuul代理
@EnableEurekaClient
public class DuduZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(DuduZuulApplication.class, args);
    }

}
