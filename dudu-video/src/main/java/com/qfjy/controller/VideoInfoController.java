package com.qfjy.controller;

import com.qfjy.service.VideoInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName VideoInfoController
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/18
 * @Version 1.0
 */
@RequestMapping("videoInfo")

@Slf4j
@RestController  // @Controller
@RefreshScope  //@RefreshScope的Bean都是延迟加载的 可以动态的更新代码中引用的配置文件的配置,或者本地的配置
public class VideoInfoController {

    @Value("${server.port}")
    private String port;

    @Autowired
    private VideoInfoService videoInfoService;

    @GetMapping("info")    //  videoInfo/info?pcode=11111
    public String selectVideoInfoByPcode(@RequestParam("pcode") String pcode){
        return videoInfoService.selectVideoInfoByPcode(pcode)+"：端口号是："+port;
    }

    @GetMapping("info1")    //  videoInfo/info?pcode=11111
    public String selectVideoInfoByPcode1(@RequestParam("pcode") String pcode){
        return videoInfoService.selectVideoInfoByPcode(pcode)+"：端口号是："+port;
    }

    @GetMapping("info2")    //  videoInfo/info?pcode=11111
    public String selectVideoInfoByPcode2(@RequestParam("pcode") String pcode){
        return videoInfoService.selectVideoInfoByPcode(pcode)+"：端口号是："+port;
    }

    @Value("${info.name}")
    private String infoName;

    @GetMapping("testInfo")
    @ResponseBody
    public String  getTestInfo(){

        return infoName;
    }


}
