package com.qfjy.service;

public interface VideoInfoService {
    /**
     * 根据会议编号 查询视频相关召开信息。
     * 分布式ID 编号（会议编号）
     */
    public String selectVideoInfoByPcode(String pcode);
}
